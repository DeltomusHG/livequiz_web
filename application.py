# -*- coding: utf-8 -*-
from flask import Flask, render_template, session, request, flash, redirect, url_for, make_response, session 
import requests
import traceback
import forms
token = ''


app = Flask(__name__)
app.config.from_object("config")
app.secret_key = 'the-fuckin-secret-key'

@app.route('/login', methods = ['GET','POST'])
@app.route('/', methods = ['GET','POST'])
def login():
    global token
    login_form = forms.login_form(request.form)
    if request.method == 'POST' and login_form.validate():
        user_email = login_form.email.data
        user_email.lower()
        response = requests.post('http://34.207.240.87:4000/api/v1/login', data = {'email':'{}'.format(user_email), 'password':'123'})
        response_data = response.json()
        token = response_data["data"]["token"]
        print token
        if "data" in response.json():
            session['user_email'] = login_form.email.data
            session['token'] = response_data["data"]["token"]
            session['user_type'] = response_data["data"]["user"]["user_type_id"]
            session['id'] = response_data["data"]["user"]["id"]
            return redirect(url_for('home'))
            print('redirected')
        elif "error" in response.json():
            print('Correo no válido')
    return render_template('login.html',form = login_form)

@app.route('/logout')
def logout():
    if 'user_email' in session:
        session.pop('user_email')
    return redirect(url_for('login'))

@app.route('/home')
def home():
    return render_template('home.html',token = token)

@app.route('/submit-test')
def submit_test():
    return render_template('submit-test.html')

@app.route('/submit-students')
def submit_students():
    return render_template('submit-students.html')


@app.route('/reports-subject')
def generate_reports_subject():
    return render_template('reports-subject.html')

@app.route('/pretest')
def pretest():
    if 'token' in session:
        token = session['token']
        print token
    return render_template('pretest.html', page_name = 'Pre - Tes',token = token)

@app.route('/livetest')
def livetest():
    if 'token' in session:
        token = session['token']
        print token
    return render_template('livetest.html', page_name = 'Live - Test')

@app.route('/posttest')
def posttest():
    if 'token' in session:
        token = session['token']
        print token
    return render_template('posttest.html', page_name = 'Post - Test')

@app.route('/team_admin')
def team_admin():
    return render_template('team_admin.html', page_name = 'Administrar equipos')

@app.route('/class_results')
def class_results():
    return render_template('class_results.html', page_name = 'Resultados de clase')

@app.route('/invite')
def invite():
    return render_template('invite.html', page_name = 'Invitar a un alumno')




if __name__ == '__main__':
    app.debug = True
    app.run()
