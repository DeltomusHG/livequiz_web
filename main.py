# -*- coding: utf-8 -*-
import sys,os,requests
from flask import Flask, render_template, session, request, flash, redirect, url_for, make_response, session
from collections import OrderedDict
from operator import itemgetter, attrgetter, methodcaller
sys.path.insert(0,'scripts')
import storage, db_query, api_request, load_test, load_students, upload_students, upload_test, prepare_test_package,forms
from api_request import get_class_id
from db_query import get_class_info
from upload_students import upload_students
import simplejson as json
import lq_configuration
import s3 as permissions_s3
import export_to_s3 as send_s3
from werkzeug.datastructures import ImmutableMultiDict
import logging
from logging.handlers import RotatingFileHandler
user_type_id = 0
token = ''
user_name = ''
user_email = ''
class_info = OrderedDict()
json_cards = OrderedDict()
index = 0
pretest_id = 0
posttest_id = 0
cass_id = 0

#user_type_id 2 -> profesor
#User_type_id 3 -> Administrador
#User_type_id 4 -> Coordinador

app = Flask(__name__)
app.config.from_object("config")
app.secret_key = 'the-fuckin-secret-key'

@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html',error = e), 500

@app.route('/login', methods = ['GET','POST'])
@app.route('/', methods = ['GET','POST'])
def login():
    global token,user_email,user_type_id
    login_form = forms.login_form(request.form)
    if 'user_email' in session:
        session.clear()
    if request.method == 'POST' and login_form.validate():
        user_email = login_form.email.data
        user_email.lower()
        response = requests.post("{}v1/login".format(lq_configuration.API_LQ_API), data = {'email':'{}'.format(user_email), 'password':'123'})
        response_data = response.json()
        #print response 
        if "data" in response.json():
            app.logger.info('login correcto del usuario')
            user_type_id = response_data["data"]["user"]["user_type_id"]
            token = response_data["data"]["token"]
            #print 'token {}'.format(token)
            session['user_email'] = login_form.email.data
            session['token'] = response_data["data"]["token"]
            session['user_type'] = response_data["data"]["user"]["user_type_id"]
            session['id'] = response_data["data"]["user"]["id"]
            
        elif "error" in response.json():
            #print('Correo no válido')
            render_template('login.html',form = login_form,message="Correo no existe.")
        if user_type_id == 2:
            return redirect(url_for('class_selector'))
        elif user_type_id == 3 or user_type_id == 4 or user_type_id == 5:
            return redirect(url_for('home_admin'))
        else:
            render_template('login.html',form = login_form)
    return(render_template('login.html',form = login_form))

@app.route('/logout')
def logout():
    if 'user_email' in session:
        session.clear()
    return redirect(url_for('login'))

@app.route('/class_selector',methods = ['GET','POST'])
def class_selector():
    if 'token' in session:
        token = session['token']
        classes = json.loads(get_class_id(token))
        #print classes
        return render_template('class_selector.html',token = token,classes = classes['data']['classes'])
    else:
        return redirect(url_for('login'))
    

@app.route('/home_professor',methods = ['GET','POST'])
def home_professor():
    class_id = request.args.get('class_id',default = 0,type = int)
    if class_id != 0:        
        global class_info,user_name,pretest_id,posttest_id,user_type_id
        #print 'Class id: {}'.format(class_id)
        class_info = get_class_info(class_id)
        print (class_info)
        #user_name = db_query.get_proffessor_name(user_email)
        user_name = 'Profesor'
        if class_info['pretest_id'] != 'None':
            pretest_id = class_info['pretest_id']
        else:
            pretest_id = 0
        if class_info['posttest_id'] != 'None':
            posttest_id = class_info['pretest_id']
        else:
            pretest_id = 0
        posttest_id = class_info['posttest_id']
        return render_template('home_professor.html',token = token,user_name = user_name,user_type_id = user_type_id)
    else:
        return redirect(url_for('class_selector'))

@app.route('/home_admin')
def home_admin():
    if 'token' in session:
        token = session['token']
        return render_template('home_admin.html',token = token,user_name = user_name,user_type_id = user_type_id)
    else:
        return redirect(url_for('login'))



@app.route('/reports-subject')
def generate_reports_subject():
    return render_template('reports-subject.html')

@app.route('/pretest')
def pretest():
    if 'token' in session:
        token = session['token']
        #print token
    return render_template('pretest.html', page_name = 'Pre - Test de clase',token = token)

@app.route('/pretest_start/',methods=['POST'])
def pretest_start():
    global pretest_id
    pretest_id = class_info['pretest_id']
    api_request.launch_pretest(class_info['class_id'],pretest_id,token)
    
    return "done"

@app.route('/pretest_stop/',methods=['POST'])
def pretest_stop():
    global pretest_id
    pretest_id = class_info['pretest_id']
    api_request.stop_pretest(class_info['class_id'],pretest_id,token)    
    return "done"

@app.route('/pretest_results/',methods = ['POST'])
def pretest_results():
    id_list = db_query.get_question_id_list(class_info['pretest_id'])
    total_percentage = 0
    for question_id in id_list:
        api_response = json.loads(api_request.get_pretest_results(class_info['class_id'],pretest_id,question_id['id'],token))
        answers = api_response['data']['answers']
        for answer in answers:
            if answer['id'] == api_response['data']['correct_answer']:
                percentage = answer['percent'] - 0.005
                #print percentage
                total_percentage = total_percentage + percentage
        
    total_percentage = total_percentage/len(id_list)
    return '{}'.format(total_percentage)




@app.route('/livetest')
def livetest():
    global json_cards,index,questions_cards
    answer_dict = OrderedDict()
    question_dict = OrderedDict()
    storage.download(class_info['gzip'])
    question_list = []
    json_cards = json.loads(open('json_test/{}.json'.format(class_info['gzip'][:-5])).read())
    question_dict = sort_questions(json_cards["data"]["series"])

    app.logger.info('question_dict %s', question_dict)
    app.logger.info('pretest_id %s postest_id %s', pretest_id, posttest_id)
    for id in question_dict:
        for card in json_cards["data"]["series"]:
            if card["id"]!= pretest_id and card["id"] != posttest_id:
                if card["questions"][0]["id"] == id:
                    #print card["questions"][0]["id"]
                    print (card)
                    questions_cards = OrderedDict()
                    questions_cards["question"] = card["description"].encode('utf-8')
                    questions_cards["id"] = card["questions"][0]["id"]
                    answer_dict = sort(card["questions"][0]["answers"])            
                    questions_cards["answers"] = answer_dict
                    question_list.append(questions_cards)

    j = json.dumps(question_list)
    with open('json_test/{}.json'.format(class_info['gzip'][:-5]), 'w') as f:
        f.write(j)
        #print 'Archivo creado'
    json_cards = json.loads(open('json_test/{}.json'.format(class_info['gzip'][:-5])).read())    
    return render_template('livetest.html', page_name = 'Live - Quiz',json_cards = json_cards,class_info = class_info,token=token)

def sort_questions(question_dict):
    question_id_list = []
    for question in question_dict:
        question_id_list.append(int(question["questions"][0]["id"]))

    question_id_list.sort()
    return question_id_list

def sort(answer_dict):
    sorted_answers = []
    id_list = []
    for answer in answer_dict:
        id_list.append(int(answer['id']))

    id_list.sort()
    for id in id_list:
        answer = OrderedDict()
        answer['id'] = id
        answer['answer'] = get_answer_data(id,answer_dict)
        sorted_answers.append(answer)
    return sorted_answers

def get_answer_data(answer_id,answer_dict):
    for answer in answer_dict:
        if answer['id'] == answer_id:
            return answer['answer']

@app.route('/get_question_results/', methods = ['POST'])
def get_question_results():
    #print request
    db_response = OrderedDict()
    api_response = OrderedDict()
    question_id = int(format(request.get_data().replace('question_id=','')))
    db_response = db_query.get_question_info(question_id)
    if 'question_id' in db_response:
        # print class_info['class_id']
        # print db_response['sections_id']
        # print db_response['question_id']
        # print db_response['correct_answer_id']
        api_response = api_request.get_realtime_results(class_info['class_id'],db_response['sections_id'],db_response['question_id'],token)
    #print api_response
    return api_response

@app.route('/restart_question_timer/', methods=['POST'])
def restart_question_timer():  
    question_id = int(format(request.get_data().replace('question_id=','')))
    api_request.pause_time(class_info['class_id'],question_id,token)
    return "done"

@app.route('/pause_question_timer/', methods=['POST'])
def pause_question_timer(): 
    question_id = int(format(request.get_data().replace('question_id=','')))
    api_request.restart_time(class_info['class_id'],question_id,token)
    return "done"

@app.route('/send_question_ajax/', methods=['POST'])
def send_question_ajax():
    #print request.get_data()
    question_id = request.get_data().replace('question_id=','')
    #print (int(question_id),class_info['class_id'])     
    api_request.send_question(class_info['class_id'],int(question_id),token)
    return "done"

@app.route('/posttest')
def posttest():
    if 'token' in session:
        token = session['token']
        #print token
    return render_template('posttest.html', page_name = 'Post - Test')

@app.route('/posttest_start/',methods=['POST'])
def posttest_start():
    global posttest_id
    posttest_id = class_info['posttest_id']
    api_request.launch_posttest(class_info['class_id'],posttest_id,token)
    #print posttest_id
    return "done"

@app.route('/posttest_stop/',methods=['POST'])
def posttest_stop():
    global posttest_id
    posttest_id = class_info['posttest_id']
    api_request.stop_posttest(class_info['class_id'],posttest_id,token)
    #print posttest_id
    return "done"

@app.route('/posttest_results/',methods = ['POST'])
def postest_results():
    id_list = db_query.get_question_id_list(class_info['posttest_id'])
    total_percentage = 0
    for question_id in id_list:
        api_response = json.loads(api_request.get_posttest_results(class_info['class_id'],posttest_id,question_id['id'],token))
        answers = api_response['data']['answers']
        for answer in answers:

            if answer['id'] == api_response['data']['correct_answer']:
                percentage = answer['percent']
                #print percentage
                total_percentage = (total_percentage + percentage) - 0.005
        
    total_percentage = total_percentage/len(id_list)
    return '{}'.format(total_percentage)

@app.route('/team_admin')
def team_admin(): 
    return render_template('team_admin.html', page_name = 'Administrar equipos')

@app.route('/class_results')
def class_results():
    return render_template('class_results.html', page_name = 'Resultados de clase')

@app.route('/invite')
def invite():
    return render_template('invite.html', page_name = 'Invitar a un alumno')

@app.route('/create_class',methods=['GET','POST'])
def create_class():
    groups_list = db_query.get_groups_list()
    subject_list = db_query.get_subjects_list()
    if request.method == 'POST':
        class_date = request.form['date-input']
        professor_email = request.form['email']
        professor_name = request.form['professor_name']

        
        group_id = request.form['group-selector']
        subject_id = request.form['subject-selector'].encode('utf-8')
        subject_name = ""

        #for group in groups_list:
            #if request.form['group-selector'] == group['name']:
                #group_id = group['id']
        for subject in subject_list:
            print( "iterando")
            print( subject['id'])
            if int(subject_id) == int(subject['id']):
                #subject_id = subject['id']
                subject_name = subject['name'].encode('utf-8')


        app.logger.info('group_id %s', group_id)
        app.logger.info('subject_id %s', subject_id)
        app.logger.info('subject_name %s', subject_name)

        response = api_request.create_class(group_id,subject_name,subject_id,professor_name,professor_email,class_date,token)

        app.logger.info('create_class_response %s', response)
        
        
        if response.status_code == requests.codes.created:
            app.logger.info('response correcto')

            response_data = response.json()
        
            #print response 
            if "data" in response.json():
                class_id = response_data["data"]["id"]

                app.logger.info('class_id %s', class_id)
                
                response = api_request.create_zip_class(class_id, token)

                app.logger.info('create_zip_class_response_status_code %s', response.status_code)
                app.logger.info('create_zip_class_response %s', response)
                
                ########################
                # NOTE PASAR GZIP A S3 #
                ########################
                gzip_name = 'class-{}.gzip'.format(class_id)
                send_s3.send(gzip_name)
                 
                ###############################################
                # NOTE ASIGNAR METADATA Y PERMISOS AL ARCHIVO #
                ###############################################
                permissions_s3.give_permissions(gzip_name)

                ############################################################
                # TODO SUBIR EL NOMBRE DEL PAQUETE A PACKAGE_URL EN LA BDD #
                ############################################################

                

    return render_template('create_class.html',groups_list = groups_list,subject_list = subject_list)

@app.route('/load_tests',methods=['GET','POST'])
def load_tests():
    subject_list = db_query.get_subjects_list()

    if 'token' in session:
        token = session['token']
    else:
        return redirect(url_for('login'))

    if request.method == 'POST':
        message = 'success'
        files = request.files.getlist('file') #Obtiene una lista de todos los archivos recibidos
        # print request.form.get('subject_input')
        subject_id = request.form.get('subject_input')
        type_examen_input = request.form.get('type_examen_input')
        #subject = request.form('subject_input')
        #print subject
        for file in files:
            app.logger.info('>>>1')
            if file.mimetype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': #Veriica que sean Excel
                folder = os.path.realpath(__file__).replace('\\','/').split('/')[:-1] #Prepara la ruta de almacenamiento
                file_path = '/'.join(folder)+'/uploads/'+file.filename
                app.logger.info('>>>2')
                #print file_path
                app.logger.info('>>>3')
                app.logger.info('>>>4')
                file.save(file_path) #Almacena el archivo
                #print ">>>4"
                test = load_test.load_test(file_path, subject_id, type_examen_input) #Convierte el archivo a JSON y lo guarda en test
                
                app.logger.info('>>subject_id %s', subject_id)
                app.logger.info('>>type_examen_input %s', type_examen_input)
                app.logger.info('>>test %s', test)

                #Invocar al servicio de carga de preguntas para examen.

                response = upload_test.upload_test(token,test) #Envía el JSON a través del servicio para ser escrito en la base de datos
                app.logger.info('>>response upload file %s', response.text)

        return render_template('load_tests.html',subject_list = subject_list,message = message)
        
    return render_template('load_tests.html',subject_list = subject_list)


    # if request.method == 'POST':
    #     f = request.files['file']
    #     group_name = request.form['group_input']
    #     print f.mimetype

    #     if f.mimetype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' and group_name!='':
    #         folder = os.path.realpath(__file__).replace('\\','/').split('/')[:-1]
    #         f.save('/'.join(folder)+'/documents/'+f.filename)
    #         load_test.load_test(f.filename)
    #         return render_template('create_groups.html')
    #     else:
    #         return render_template('create_groups.html',message='error')
    
    # return render_template('load_tests.html',subject_list = subject_list)

@app.route('/create_groups',methods=['GET','POST'])
def create_groups():
    if request.method == 'POST':
        f = request.files['file']
        group_name = request.form['group_input']
        #print group_name
        if f.mimetype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' and group_name!='':
            folder = os.path.realpath(__file__).replace('\\','/').split('/')[:-1]
            f.save('/'.join(folder)+'/documents/'+f.filename)
            json_students = json.loads(load_students.load_students(f.filename))
            #upload_students.students_to_users_details(json_students['students'])
            upload_students(json_students['students'],group_name)
            return render_template('create_groups.html')
        else:
            return render_template('create_groups.html',message='error')
    return render_template('create_groups.html')


@app.route('/edit_group')
def edit_group():
    return render_template('edit_group.html')
    


if __name__ == '__main__':
    app.debug = True
    app.run(host='127.0.0.1', port=8000)

handler = RotatingFileHandler('lq.log', maxBytes=10000, backupCount=1)
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)