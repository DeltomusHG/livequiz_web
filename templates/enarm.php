<?php
$results = array(
	array('name' => 'Anatomía Patológica',
        'r_13' => 63.777,
        'r_14' => 64.666,
        'r_15' => 66.223,
        'r_16' => 68.000,
        'r_17' => 68.667),
	array('name' => 'Anestesiología',
        'r_13' => 64.223,
        'r_14' => 66.666,
        'r_15' => 66.889,
        'r_16' => 66.889,
        'r_17' => 68.000)
    );


if(isset($_POST['submit'])){
    $especialidad = $_POST['especialidades'];  // Storing Selected Value In Variable
    $puntaje = $_POST['number_input'];
  	switch($especialidad){
        case "Anatomía Patológica":
      	    get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
        case "Anestesiología":
            get_results_years($especialidad,$puntaje,$results);
        	get_results_2017($puntaje,$results);
            break;
    
        }
    } 

function get_results_years($esp,$pun,$results){
    $index = array_search($esp, array_column($results, 'name'));
    echo '<div style="border: solid #f7f7f9;">';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-lg-4 col-sm-12" style="text-align: center;">';
    echo 'Especialidad: '.$esp;
    echo '</div>';
    echo '<div class="col-lg-4 col-sm-12" style="text-align: center;">';
    echo 'Puntaje obtenido: '.$pun;
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-lg-6 col-md-12 col-sm-12" style="text-align: center;">';
    echo '<ul class="list-group list-group-flush" style="text-align: center; width:100%;">';
  
    if($pun >= $results[$index]['r_13']){
      	$min = $results[$index]['r_13'];
      	echo '<li class="list-group-item justify-content-center align-items-center">';
        echo 'Aceptado en 2013';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    } else {
        $min = $results[$index]['r_13'];
        echo '<li class="list-group-item d-flex justify-content-center align-items-center">';
        echo 'No aceptado en 2013';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
    if($pun >= $results[$index]['r_14']){
        $min = $results[$index]['r_14'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
        echo 'Aceptado en 2014';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    } else {
        $min = $results[$index]['r_14'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
        echo 'No aceptado en 2014';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
    if($pun >= $results[$index]['r_15']){
        $min = $results[$index]['r_15'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
        echo 'Aceptado en 2015';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    } else {
        $min = $results[$index]['r_15'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
        echo 'No aceptado en 2015';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
    if($pun >= $results[$index]['r_16']){
        $min = $results[$index]['r_16'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
        echo 'Aceptado en 2016';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    } else {
        $min = $results[$index]['r_16'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
        echo 'No aceptado en 2016';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
    if($pun >= $results[$index]['r_17']){
        $min = $results[$index]['r_17'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
        echo 'Aceptado en 2017';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    } else {
        $min = $results[$index]['r_17'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
        echo 'No aceptado en 2017';
      	echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
      	echo '</li>';
    }
    echo '</ul>';
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
    echo '<div class="row">';
    echo '<div class="col" style="height:30px;"></div>';
    echo '</div>';
    echo '</div>';
    
    //echo $results[$index]['r_13'].'</br>';
    //echo $results[$index]['r_14'].'</br>';
    //echo $results[$index]['r_15'].'</br>';
    //echo $results[$index]['r_16'].'</br>';
    //echo $results[$index]['r_17'].'</br>';
    }

function get_results_2017($pun,$results){
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-lg-6 col-sm-12" style="text-align: center;">';
    echo 'Tu puntaje pudo haberte colocado en las siguientes especialidades: </br>';
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
    echo '<div class="row">';
    echo '<div class="col"></div>';
    echo '<div class="col-lg-6 col-sm-12" style="text-align: center;">';
    echo '<ul class="list-group list-group-flush">';
  
  foreach($results as $res){
    if($pun >= $res['r_17']){
        $min = $res['r_17'];
        echo '<li class="list-group-item d-flex justify-content-between align-items-center">';
        echo $res['name'].'</br>';
        echo '<span class="badge badge-primary badge-pill">Min '.$min.'</span>';
        echo '</li>';
    }
  	
      
  }
    echo '</ul>';
    echo '</div>';
    echo '<div class="col"></div>';
    echo '</div>';
    
}







?>