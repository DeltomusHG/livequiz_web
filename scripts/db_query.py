# -*- coding: utf-8 -*-
import psycopg2, psycopg2.extras, xlwt, xlrd, json, lq_configuration
from collections import OrderedDict
from datetime import datetime
from api_request import get_class_id

host = lq_configuration.DB_HOST
user = lq_configuration.DB_USER
database = lq_configuration.DB_SCHEMA
password = lq_configuration.DB_PASSWORD



def get_class_info(class_id):
    try:
        conn = psycopg2.connect(database = database, user = user, password = password, host = host )
        cur = conn.cursor()
    except:
        return 'Imposible completar operación'

    try:
        #cur.execute("select id,name,package_url,pre_test_series_id,post_test_series_id from classes where id = (select classes_id from classes_users where user_id = (select id from users where user_type_id = 2 and email = '{}'))".format(email))
        cur.execute("select id,name,package_url,pre_test_series_id,post_test_series_id from classes where id = {}".format(class_id))
        class_info = OrderedDict() #Hay que reparar la class info
        for row in cur:
            print (row)
            class_info['class_id'] = int(row[0])
            class_info['class_name'] = unicode(row[1],'utf-8')
            class_info['gzip'] = row[2]
            class_info['pretest_id'] = int(row[3])
            class_info['posttest_id'] = int(row[4])
        cur.close()
        return class_info
    except Exception as e:
        return e
    
def get_proffessor_name(email):
    try:
        conn = psycopg2.connect(database = database, user = user, password = password, host = host)
        cur = conn.cursor()
    except:
        return 'Imposible completar operación'

    cur.execute("select display_name from users where user_type_id = 2 and email = '{}'".format(email))
    for i,row in enumerate(cur):
        proffesor_name = unicode(row[0],'utf-8')
    cur.close()
    return proffesor_name

def get_groups_list():
    groups_list = []
    try:
        conn = psycopg2.connect(database = database, user = user, password = password, host = host)
        cur = conn.cursor()
    except:
        return 'Imposible completar operación'

    cur.execute("select id,name from class_groups")
    for row in cur:
        group = OrderedDict()
        group['id'] = int(row[0])
        group['name'] = unicode(row[1],'utf-8')
        groups_list.append(group)
    cur.close()
    return groups_list

def get_subjects_list():
    subject_list = []
    try:
        conn = psycopg2.connect(database = database, user = user, password = password, host = host)
        cur = conn.cursor()
    except Exception as e:
        print(e.message)
        print (">>ERROR en conexion db")
        return 'Imposible completar operación'

    cur.execute("select id,name from subjects")

    print (">>>>>")
    for row in cur:
        print ("iterando ....  >>>>")
        subject = OrderedDict()
        subject['id'] = int(row[0])
        subject['name'] = unicode(row[1],'utf-8')
        #print subject['name'].encode('utf-8')
        subject_list.append(subject)

    cur.close()
    return subject_list

def get_question_info(question_id):
    db_response = OrderedDict()
    try:
        conn = psycopg2.connect(database = database, user = user, password = password, host = host)
        cur = conn.cursor()
    except:
        return 'Imposible completar operación'
    try:
        cur.execute("""
        SELECT id,sections_id,correct_answer_id
        FROM questions
        WHERE id = {}
        """.format(question_id))
        for row in cur:
            db_response['question_id'] = int(row[0])
            db_response['sections_id'] = int(row[1])
            db_response['correct_answer_id'] = int(row[2])    
    except:
        db_response['error'] = "Error: No se ha realizado la consulta correctamente."
        print ("Error")
    cur.close()
    return db_response       

def get_question_id_list(sections_id):
    id_list = []
    try:
        conn = psycopg2.connect(database = database, user = user, password = password, host = host)
        cur = conn.cursor()
    except:
        return 'Imposible completar operación'
    try:
        cur.execute("""
        SELECT id 
        FROM questions 
        WHERE sections_id={}
        """.format(sections_id))
        for row in cur:
            db_response = OrderedDict()
            db_response['id'] = int(row[0])
            id_list.append(db_response)
    except:
        db_response = OrderedDict()
        db_response['error'] = "Error: No se ha realizado la consulta correctamente."
        id_list.append(db_response)
        print ('Error')

    cur.close()
    return id_list


#Creating db connection and cursor


#print get_subjects_list()
#print get_groups_list()
#email = raw_input()
#print get_class_info(email)
#print get_proffessor_name(email)
#print get_question_id_list(874)



#get_class_info(raw_input("class_id: "))