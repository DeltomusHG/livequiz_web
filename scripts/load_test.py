#! /usr/bin/env python
# -*- coding: utf-8 -*-
import xlrd
from collections import OrderedDict
import simplejson as json
import requests
question_number = 1
test_ok = True
error = OrderedDict()

def test_validator(row_values):
    if row_values[6].lower() != 'a' and row_values[6].lower() != 'b' and row_values[6].lower() != 'c' and row_values[6].lower() != 'd' and row_values[2] != '':
        return False
    else:
        return test_ok


def load_test(filename, subject_id, test_type):
    """ Metodo que se encarga de realizar la conversion del archivo de excel a un json, para enviarlo al servicio web.
    
    Keyword arguments:
    filename -- Ruta al archivo.
    subject_id -- id de la materia.
    test_type -- tipo de test.
    """

    global question_number, test_ok,error
    test_ok = True
    question_number = 1
    #print filename.encode('utf-8')
    #print filename.decode('utf-8')
    #filename = filename.decode('utf-8')
    #filename = filename.encode("ascii")
    filename = filename.encode('utf8','replace')
    #wb = xlrd.open_workbook('../documents/{}'.format(filename)) #Cuando ejecutamos el script localmente desde la carpeta scripts
    wb = xlrd.open_workbook(filename) #Cuando ejecutamos el script desde el main
    #wb = xlrd.open_workbook('{}'.format(filename))
    sh = wb.sheet_by_index(0)
    rownum = 2

############ Listas que contienen a los diccionarios ############
    question_list = []
    serial_question_list = []
    serial_question_container = []
    normal_question_list = []
############ Fin de las listas ############
    counter = 0
# Construyendo un iterador
    simple_list = []
    for rownum in range(6, sh.nrows):
        simple_list.append(rownum)
    iterator = iter(simple_list)

    test_name = sh.cell(2,1).value.encode('utf-8')
    print(test_name)

    def standardize_content(content_string):
        index = content_string.find('. ')
        if index <= 7 and index > 0:
            content_string = content_string[ index + 2 : ]
            return content_string
        else:
            return content_string

    def answers(row):
        #filtro de tipo de contenido, para ser procesado diferente
        answers = OrderedDict()
        answers['answer_a'] = standardize_content(unicode(row[2]).encode('utf-8').replace('\r', '').replace('\n', ' '))
        answers['answer_b'] = standardize_content(unicode(row[3]).encode('utf-8').replace('\r', '').replace('\n', ' '))
        answers['answer_c'] = standardize_content(unicode(row[4]).encode('utf-8').replace('\r', '').replace('\n', ' '))
        answers['answer_d'] = standardize_content(unicode(row[5]).encode('utf-8').replace('\r', '').replace('\n', ' '))
        answers['correct_answer'] = "answer_{ca}".format(ca=row[6].lower())
        return answers
    
    for rownum in iterator:
        row_values = sh.row_values(rownum)
        question = OrderedDict()
        serial_question = OrderedDict()
        test_ok = test_validator(row_values)

        if row_values[0] == '' and row_values[1] == '' and row_values[2] == '' and row_values[3] == '' and row_values[4] == '' and row_values[5] == '' and row_values[6] == '':
            break
   

        if test_ok == True:
            # if row_values[0] != '':
            
            #     id_series = int(row_values[0])
            #     serial_question['id'] = counter + 1
            #     serial_question['question_number'] = question_number
            #     serial_question['question'] = row_values[1].replace('\r', '').replace('\n', ' ')
            #     serial_question['answers'] = answers(row_values)
            #     serial_question_list.append(serial_question)
            #     question_number += 1
            #     #print rownum,sh.nrows
            #     if rownum < sh.nrows-1:
            #         if type(sh.row_values(rownum + 1)[0]) == unicode or int(sh.row_values(rownum + 1)[0]) != id_series:
            #             question['series'] = serial_question_list
            #             serial_question_container.append(question)
            #             serial_question_list = []

            # elif type(row_values[0]) == float:
            #
            serial_question_list = []
            serial_question['id'] = counter + 1
            serial_question['question_number'] = question_number
            serial_question['question'] = row_values[1].replace('\r', '').replace('\n', ' ')
            serial_question['answers'] = answers(row_values)
            serial_question_list.append(serial_question)
            question['series'] = serial_question_list
            normal_question_list.append(question)
            serial_question_list = []
            question_number += 1
            counter += 1


        elif test_ok == False:
            #print rownum
            error['error:'] = 'Error loading test file.'
            error['question:'] = '{}'.format(row_values[1].encode('utf-8'))
            if row_values[6] == '':
                error['type:'] = 'Empty answer'
            elif row_values[6] != '':
                error['type:'] = 'Non valid answer {}'.format(row_values[6])
            break

        if row_values[0] == '' and row_values[1] == '' and row_values[2] == '' and row_values[3] == '' and row_values[4] == '' and row_values[5] == '' and row_values[6] == '':
            break
           
        


        all_question = OrderedDict()
        test = []
        test_dict = OrderedDict()
        #all_question['id_class'] = 19
        all_question['subject_id'] = subject_id
        all_question['test_name'] = test_name
        all_question['test_type'] = test_type
        #all_question['test_type_name'] = '{}-TEST'.format(sh.cell(2,3).value.upper())
        test_dict['normal_questions'] = normal_question_list 
        test_dict['serial_questions'] = serial_question_container
        test.append(test_dict)
        all_question['test'] = test
        
    # if test_ok == True:
    #     question_list.append(all_question)
    # elif test_ok == False:
    #     question_list.append(error) 


    # Serializando las listas al JSON
    j = json.dumps(all_question)
    #print j

    ### No es necesario guardar el archivo
    #with open('json_quiz/{}_{}.json'.format(test_name.replace(' ','_'),sh.cell(2,3).value.upper()), 'w') as f: #Cuando se ejecuta el script desde el main
    #    f.write(j)
    #    print 'Archivo creado'

    # with open('../json_quiz/{}_{}.json'.format(test_name.replace(' ','_'),sh.cell(2,3).value.upper()), 'w') as f: #Cuando se ejecuta el script localmente
    #     f.write(j)
    #     print 'Archivo creado'

    # Escribiendo en el archivo

    return j



#load_test(unicode(raw_input(), 'utf8'))


#aws s3 cp /home/ec2-user/lq/lqrepo/live_quiz_be_umbrella/_build/prod/rel/lq/class-10.gzip s3://livequiz-cr-01/classes/clase_neumo.gzip