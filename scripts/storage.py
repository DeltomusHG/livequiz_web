import gzip, urllib
import simplejson as json

def download(file_name):
    testfile = urllib.URLopener()
    testfile.retrieve('https://s3.us-east-2.amazonaws.com/livequiz-cr-01/classes/{}'.format(file_name),'tmp/{}'.format(file_name))
    
    with gzip.open('tmp/{}'.format(file_name),'rt' ) as f:
        file_content = f.read()
        json_test = json.loads(file_content)
    
    j = json.dumps(json_test)
    with open('json_test/{}.json'.format(file_name[:-5]), 'w') as f:
        f.write(j)
        print 'Archivo creado'
