#! /usr/bin/env python
# -*- coding: utf-8 -*-
import xlrd
from collections import OrderedDict
import simplejson as json


def load_students(filename):
    # wb = xlrd.open_workbook('../documents/{}'.format(filename))
    wb = xlrd.open_workbook('documents/{}'.format(filename))
    sh = wb.sheet_by_index(0)

############ Listas que contienen a los diccionarios ############
    student_list = []
############ Fin de las listas ############
# Construyendo un iterador
    simple_list = []
    for rownum in range(2, sh.nrows):
        simple_list.append(rownum)
    iterator = iter(simple_list)
    group_name = sh.cell(0,1).value.encode('utf-8')
    print(group_name)
    
    for rownum in iterator:
        row_values = sh.row_values(rownum)
        students = OrderedDict()
        students['student_name'] = row_values[2].encode('utf-8')
        #surname = '{} {}'.format(row_values[0].encode('utf-8'),row_values[1].encode('utf-8'))
        #students['student_surname'] = surname
        students['student_maidenname'] = row_values[1].encode('utf-8') 
        students['student_surname'] = row_values[0].encode('utf-8')
        students['email'] = row_values[3].encode('utf-8').lower()
        student_list.append(students)

    all_students = OrderedDict()
    all_students['group'] = group_name
    all_students['students'] = student_list

# Serializando las listas al JSON
    j = json.dumps(all_students)

# Escribiendo en el archivo
    # with open('../json_quiz/{}.json'.format(group_name.upper()), 'w') as f:
    #     f.write(j)

    # return j

    with open('json_quiz/{}.json'.format(group_name.upper()), 'w') as f:
        f.write(j)

    return j

#load_students(raw_input())