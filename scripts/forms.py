#-*- coding utf-8 -*-
from wtforms import Form, StringField, TextField, validators
from wtforms.fields.html5 import EmailField


class login_form(Form):
    email = EmailField('Correo electronico',
                    [
                        validators.Required(message = 'El email es requerido'),
                        validators.Email(message = 'Ingese un correo valido.')
                    ]
                    )
 