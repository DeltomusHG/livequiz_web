# -*- coding: UTF-8 -*-
import xlwt, xlrd, json
from collections import OrderedDict
from datetime import datetime

header = xlwt.easyxf('')
title = xlwt.easyxf('')
title_small = xlwt.easyxf('')
title_tiny = xlwt.easyxf('')
report_file = ''
report_name = ''
subject = ''
group = ''
proffessor = ''
student_name = ''



##################### Loading JSON response ########################

def load_JSON():
    with open('../documents/reporte_materia_json.json') as file:
        data = json.load(file)
        return data


############ Creating Worksheet #############
def create_workbook():
    workbook = xlwt.Workbook(encoding='utf-8')
    return workbook

def create_worksheet(workbook):
    worksheet = workbook.add_sheet('RPM')
    return worksheet


############# Adding colors #################
def set_colors(wb):
    xlwt.add_palette_colour('blue_header',0x25)
    wb.set_colour_RGB(0x25,0,72,131)

    xlwt.add_palette_colour('blue_title',0x26)
    wb.set_colour_RGB(0x26, 0,113,191)

    xlwt.add_palette_colour('gray_separator',0x27)
    wb.set_colour_RGB(0x27,178,178,178)

############# Setting styles #################
def set_sheet_style():
    global header,title,title_small,title_tiny
    header = xlwt.easyxf('font: name Helvetica, color-index white, height 300;'
                        'alignment: horizontal center, vertical center;'
                        'pattern: pattern solid, fore_color blue_header;'
                        'borders: left thin, right thin, top thin, bottom thin,'
                        'left_color gray_separator, right_color gray_separator,'
                        'top_color gray_separator, bottom_color gray_separator;')

    title = xlwt.easyxf('font: name Helvetica, color-index white, height 300;'
                        'alignment: horizontal center, vertical center;'
                        'pattern: pattern solid, fore_color blue_title;'
                        'borders: left thin, right thin, top thin, bottom thin,'
                        'left_color gray_separator, right_color gray_separator,'
                        'top_color gray_separator, bottom_color gray_separator;')

    title_small = xlwt.easyxf('font: name Helvetica, color-index white, height 150;'
                        'alignment: horizontal center, vertical center;'
                        'pattern: pattern solid, fore_color blue_title;'
                        'borders: left thin, right thin, top thin, bottom thin,'
                        'left_color gray_separator, right_color gray_separator,'
                        'top_color gray_separator, bottom_color gray_separator;')


    title_tiny = xlwt.easyxf('font: name Helvetica, color-index white, height 225;'
                        'alignment: horizontal center, vertical center;'
                        'pattern: pattern solid, fore_color blue_title;'
                        'borders: left thin, right thin, top thin, bottom thin,'
                        'left_color gray_separator, right_color gray_separator,'
                        'top_color gray_separator, bottom_color gray_separator;')



#################### Sizing columns #######################
def sizing_rpm_template(ws):
    ws.col(0).width = 256 * 50
    ws.col(1).width = 256 * 15
    ws.col(2).width = 256 * 15
    ws.col(3).width = 256 * 15
    ws.col(4).width = 256 * 15
    ws.col(5).width = 256 * 15
    ws.col(6).width = 256 * 15
    ws.col(7).width = 256 * 25
    ws.col(8).width = 256 * 25
##################### Sizing rows ########################
    ws.row(0).height_mismatch = True
    ws.row(0).height = 128 * 5
    ws.row(1).height_mismatch = True
    ws.row(1).height = 256 * 2
    ws.row(2).height_mismatch = True
    ws.row(2).height = 256 * 2
    ws.row(3).height_mismatch = True
    ws.row(3).height = 256 * 2


def building_rpm_template(ws):
##################### Building template ########################
    ws.write_merge(0,0,0,8,'{}'.format(report_name))
    ws.write_merge(1,1,0,6,'{} Grupo: {}'.format(subject,group), header)
    ws.write_merge(1,1,7,8,'Profesor: {}'.format(proffessor),header)
    ws.write_merge(2,3,0,0,'Alumno', title)
    ws.write_merge(2,2,1,2,'Pre Test', title)
    ws.write_merge(2,2,3,4,'Live Quiz', title)
    ws.write_merge(2,2,5,6,'Post Test', title)
    ws.write_merge(3,3,1,1,'Correctas', title)
    ws.write_merge(3,3,2,2,'Totales', title)
    ws.write_merge(3,3,3,3,'Correctas', title)
    ws.write_merge(3,3,4,4,'Totales', title)
    ws.write_merge(3,3,5,5,'Correctas', title)
    ws.write_merge(3,3,6,6,'Totales', title)
    ws.write_merge(2,3,7,7,'Promedio Test', title)
    ws.write_merge(2,3,8,8,'Mejoría', title)


def fill_rpm_report(ws,data):
    lista_estudiantes = data['data']['classes'][0]['students']
    for i,student in enumerate(lista_estudiantes):
        ws.write(i+4,0,str(student['name']))
        ws.write(i+4,1,student['tests'][0]['correct_answers'])
        ws.write(i+4,2,student['tests'][0]['total_questions'])
        ws.write(i+4,3,student['tests'][1]['correct_answers'])
        ws.write(i+4,4,student['tests'][1]['total_questions'])
        ws.write(i+4,5,student['tests'][2]['correct_answers'])
        ws.write(i+4,6,student['tests'][2]['total_questions'])
        ws.write(i+4,7,'{0:.2f}%'.format(rpm_average_test(student)))
        ws.write(i+4,8,'{0:.2f}%'.format(rpm_improvement(student)))

        #print(str(student['name']),str(student['tests'][0]['test_type']),
                #student['tests'][0]['correct_answers'],
                #student['tests'][0]['total_questions'],i)
        
    #print(float(float(student['tests'][2]['correct_answers'])/float(student['tests'][2]['total_questions'])))

def rpm_average_test(student):
    pre = float(float(student['tests'][0]['correct_answers'])/float(student['tests'][0]['total_questions']))
    live = float(float(student['tests'][1]['correct_answers'])/float(student['tests'][1]['total_questions']))
    post = float(float(student['tests'][2]['correct_answers'])/float(student['tests'][2]['total_questions']))
    average = float((pre + live + post ) / 3) * 100
    return average

def rpm_improvement(student):
    pre = float(float(student['tests'][0]['correct_answers'])/float(student['tests'][0]['total_questions']))
    post = float(float(student['tests'][2]['correct_answers'])/float(student['tests'][2]['total_questions']))
    improvement = ( post - pre ) * 100
    return improvement
    



##################### Saving report ########################
def save_report(wb):
    wb.save('../reports/{}.xls'.format(report_file))

def get_report_info():
    global subject,group,proffessor
    data = load_JSON()
    subject = data['data']['classes'][1]['name']
    group = data['data']['classes'][0]['group_name']
    proffessor = data['data']['classes'][0]['proffessor_name']


def rpm_report():
    global report_file,report_name
    report_name = 'Respuestas de respuestas por materia.'
    report_file = 'Respuestas_por_materia'
    wb = create_workbook()
    ws = create_worksheet(wb)
    set_colors(wb)
    set_sheet_style()
    sizing_rpm_template(ws)
    get_report_info()
    building_rpm_template(ws)
    fill_rpm_report(ws,load_JSON())
    save_report(wb)

def groupkdx_report():
    global report_file,report_name
    report_name = 'Kardex Grupal'
    report_file = 'Kardex_grupo_{}'.format(group)
    wb = create_workbook()
    ws = create_worksheet(wb)
    set_colors(wb)
    set_sheet_style()
    sizing_groupkdx_template(ws)
    get_report_info()
    building_groupkdx_template(ws)
    fill_groupkdx_report(ws,load_JSON())
    save_report(wb)

def sizing_groupkdx_template(ws):
    ws.col(0).width = 256 * 25
    ws.col(1).width = 256 * 15
    ws.col(2).width = 256 * 15
    ws.col(3).width = 256 * 15
    ws.col(4).width = 256 * 20
    ws.col(5).width = 256 * 20
##################### Sizing rows ########################
    ws.row(0).height_mismatch = True
    ws.row(0).height = 128 * 5
    ws.row(1).height_mismatch = True
    ws.row(1).height = 256 * 2
    ws.row(2).height_mismatch = True
    ws.row(2).height = 256 * 2
    ws.row(3).height_mismatch = True
    ws.row(3).height = 256 * 2

def building_groupkdx_template(ws):
    ##################### Building template ########################
    ws.write_merge(0,0,0,5,'{}'.format(report_name))
    ws.write_merge(1,1,0,5,'Grupo: {}'.format(group), header)
    ws.write_merge(2,3,0,0,'Materia', title)
    ws.write_merge(2,2,1,3,'Etapa',title)
    ws.write(3,1,'Pre-Test',title)
    ws.write(3,2,'Live-Test',title)
    ws.write(3,3,'Post-Test',title)
    ws.write_merge(2,3,4,4,'Promedio Test',title)
    ws.write_merge(2,3,5,5,'Mejoria Grupal',title)

def fill_groupkdx_report(ws,data):
    subject_list = data['data']['classes']
    result_list = []
    results = OrderedDict()
    stages_list = ['Pre-Test','Live-Test','Post-Test'] 
    stages_results = []
    stages = OrderedDict()
    
    for i,subject in enumerate(subject_list):
        lista_estudiantes = data['data']['classes'][i]['students']
        pretest_counter = 0
        livetest_counter = 0
        posttest_counter = 0
        student_counter = 0
        average = 0
        improvement = 0
        for j,student in enumerate(lista_estudiantes):
            pretest_counter = pretest_counter + (float(float(student['tests'][0]['correct_answers']/float(student['tests'][0]['total_questions'])))*100)
            livetest_counter = livetest_counter + (float(float(student['tests'][1]['correct_answers']/float(student['tests'][1]['total_questions'])))*100)
            posttest_counter = posttest_counter + (float(float(student['tests'][2]['correct_answers']/float(student['tests'][2]['total_questions'])))*100)
            student_counter += 1
        pretest_counter = float(pretest_counter / student_counter)
        livetest_counter = float(livetest_counter / student_counter)
        posttest_counter = float(posttest_counter / student_counter)
        average = float((pretest_counter + livetest_counter + posttest_counter)/3)
        improvement = float(posttest_counter - pretest_counter)
        ws.write(i+4,0,subject['name'])
        ws.write(i+4,1,'{0:.2f}%'.format(pretest_counter))
        ws.write(i+4,2,'{0:.2f}%'.format(livetest_counter))
        ws.write(i+4,3,'{0:.2f}%'.format(posttest_counter))
        ws.write(i+4,4,'{0:.2f}%'.format(average))
        ws.write(i+4,5,'{0:.2f}%'.format(improvement))

def indkdx_report():
    global report_file,report_name,student_name
    report_name = 'Kardex de alumno'
    student_name = 'Stephen Hawking'
    report_file = 'Kardex_alumno_{}'.format(student_name)
    wb = create_workbook()
    ws = create_worksheet(wb)
    set_colors(wb)
    set_sheet_style()
    sizing_indkdx_template(ws)
    get_report_info()
    building_indkdx_template(ws)
    fill_indkdx_report(ws,load_JSON(),student_name)
    save_report(wb)

def sizing_indkdx_template(ws):
    ws.col(0).width = 256 * 25
    ws.col(1).width = 256 * 8
    ws.col(2).width = 256 * 8
    ws.col(3).width = 256 * 8
    ws.col(4).width = 256 * 8
    ws.col(5).width = 256 * 8
    ws.col(6).width = 256 * 8
    ws.col(7).width = 256 * 8
    ws.col(8).width = 256 * 8
    ws.col(9).width = 256 * 8
    ws.col(10).width = 256 * 8
    ws.col(11).width = 256 * 8
    ws.col(12).width = 256 * 8
    ws.col(13).width = 256 * 20
    ws.col(14).width = 256 * 20
##################### Sizing rows ########################
    ws.row(0).height_mismatch = True
    ws.row(0).height = 128 * 5
    ws.row(1).height_mismatch = True
    ws.row(1).height = 256 * 2
    ws.row(2).height_mismatch = True
    ws.row(2).height = 256 * 2
    ws.row(3).height_mismatch = True
    ws.row(3).height = 256 * 2

def building_indkdx_template(ws):
    ws.write_merge(0,0,0,14,'{}'.format(report_name))
    ws.write_merge(1,1,0,12,'Alumno: {}'.format(student_name), header)
    ws.write_merge(1,1,13,14,'Grupo: {}'.format(group),header)
    ws.write_merge(2,3,0,0,'Materia', title)
    ws.write_merge(2,2,1,4,'Pre-Test',title)
    ws.write(3,1,'Correctas',title_small)
    ws.write(3,2,'Incorrectas',title_small)
    ws.write(3,3,'Sin responder',title_small)
    ws.write(3,4,'Totales',title_small) 
    ws.write_merge(2,2,5,8,'Live-Test',title)
    ws.write(3,5,'Correctas',title_small)
    ws.write(3,6,'Incorrectas',title_small)
    ws.write(3,7,'Sin responder',title_small)
    ws.write(3,8,'Totales',title_small)
    ws.write_merge(2,2,9,12,'Post-Test',title)
    ws.write(3,9,'Correctas',title_small)
    ws.write(3,10,'Incorrectas',title_small)
    ws.write(3,11,'Sin responder',title_small)
    ws.write(3,12,'Totales',title_small)
    ws.write_merge(2,3,13,13,'Mejoria',title)
    ws.write_merge(2,3,14,14,'Promedio',title)

def fill_indkdx_report(ws,data,student_name):
    subject_list = data['data']['classes']
    result_list = []
    results = OrderedDict()
    stages_list = ['Pre-Test','Live-Test','Post-Test'] 
    stages_results = []
    stages = OrderedDict()
    row = 0
    
    for i,subject in enumerate(subject_list):
        lista_estudiantes = data['data']['classes'][i]['students']
        pretest_correct = 0
        livetest_correct = 0
        posttest_correct = 0
        student_counter = 0
        average = 0
        improvement = 0
        for j,student in enumerate(lista_estudiantes):
            if student['name'] == student_name:
                pretest_percentage = (float(float(student['tests'][0]['correct_answers']/float(student['tests'][0]['total_questions'])))*100)
                livetest_percentage = (float(float(student['tests'][1]['correct_answers']/float(student['tests'][1]['total_questions'])))*100)
                posttest_percentage = (float(float(student['tests'][2]['correct_answers']/float(student['tests'][2]['total_questions'])))*100) 
                pretest_correct = student['tests'][0]['correct_answers']
                livetest_correct = student['tests'][1]['correct_answers']
                posttest_correct= student['tests'][2]['correct_answers']
                pretest_incorrect = student['tests'][0]['incorrect_answers']
                livetest_incorrect = student['tests'][1]['incorrect_answers']
                posttest_incorrect= student['tests'][2]['incorrect_answers']
                pretest_total = student['tests'][0]['total_questions']
                livetest_total = student['tests'][1]['total_questions']
                posttest_total = student['tests'][2]['total_questions']
                average = float((pretest_percentage + livetest_percentage + posttest_percentage)/3)
                improvement = float(posttest_percentage - pretest_percentage)
                ws.write(row+4,0,subject['name'])
                ws.write(row+4,1,pretest_correct)
                ws.write(row+4,5,livetest_correct)
                ws.write(row+4,9,posttest_correct)
                ws.write(row+4,2,pretest_incorrect)
                ws.write(row+4,6,livetest_incorrect)
                ws.write(row+4,10,posttest_incorrect)
                ws.write(row+4,3,pretest_total-(pretest_correct + pretest_incorrect))
                ws.write(row+4,7,livetest_total-(livetest_correct + livetest_incorrect))
                ws.write(row+4,11,posttest_total-(posttest_correct + posttest_incorrect))
                ws.write(row+4,4,pretest_total)
                ws.write(row+4,8,livetest_total)
                ws.write(row+4,12,livetest_total)
                ws.write(row+4,14,'{0:.2f}%'.format(average))
                ws.write(row+4,13,'{0:.2f}%'.format(improvement))
                row +=1

def asist_report():
    global report_file,report_name,group
    report_name = 'Kardex de alumno'
    group = 'MIX_CDMX_NOV17'
    report_file = 'Reporte_asistencia_grupo{}'.format(group)
    wb = create_workbook()
    ws = create_worksheet(wb)
    set_colors(wb)
    set_sheet_style()
    sizing_asist_template(ws)
    get_report_info()
    building_asist_template(ws)
    fill_asist_report(ws,load_JSON())
    save_report(wb)
    

def sizing_asist_template(ws):
    ws.col(0).width = 256 * 25
    ws.col(1).width = 256 * 20
    ws.col(2).width = 256 * 20
    ws.col(3).width = 256 * 20
    ws.col(4).width = 256 * 20
##################### Sizing rows ########################
    ws.row(0).height_mismatch = True
    ws.row(0).height = 128 * 5
    ws.row(1).height_mismatch = True
    ws.row(1).height = 256 * 2
    ws.row(2).height_mismatch = True
    ws.row(2).height = 256 * 2
    ws.row(3).height_mismatch = True
    ws.row(3).height = 256 * 2

def building_asist_template(ws):
    ws.write_merge(0,0,0,4,'{}'.format(report_name))
    ws.write_merge(1,1,0,2,'Grupo: {}'.format(group), header)
    ws.write_merge(1,1,3,4,'En lista: {}'.format(total_asist()),header)
    ws.write_merge(2,3,0,0,'Materia', title)
    ws.write_merge(2,3,1,1,'Fecha',title)
    ws.write_merge(2,2,2,4,'Participación',title)
    ws.write(3,2,'Pre-Test',title_tiny)
    ws.write(3,3,'Live-Test',title_tiny)
    ws.write(3,4,'Post-Test',title_tiny)
    
def fill_asist_report(ws,data):
    subject_list = data['data']['classes']
    stages_list = ['Pre-Test','Live-Test','Post-Test'] 
    
    for i,subject in enumerate(subject_list):
        lista_estudiantes = data['data']['classes'][i]['students']
        pretest_asist = 0
        livetest_asist= 0
        posttest_asist = 0
        for j,student in enumerate(lista_estudiantes):
            if (int(student['tests'][0]['incorrect_answers']) + int(student['tests'][0]['correct_answers'])) > 0:
                pretest_asist += 1
            if (int(student['tests'][1]['incorrect_answers']) + int(student['tests'][1]['correct_answers'])) > 0:
                livetest_asist += 1
            if (int(student['tests'][2]['incorrect_answers']) + int(student['tests'][2]['correct_answers'])) > 0:
                posttest_asist += 1
        ws.write(i+4,0,'{}'.format(subject['name']))
        ws.write(i+4,2,pretest_asist)
        ws.write(i+4,3,livetest_asist)
        ws.write(i+4,4,posttest_asist)


def total_asist():
    #Requiere hacer una consulta en base de datos
    return 50




        
        




 



####################### Report calls ########################

rpm_report()
groupkdx_report()
indkdx_report()
asist_report()