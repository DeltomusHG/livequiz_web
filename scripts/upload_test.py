#! /usr/bin/env python
# -*- coding: utf-8 -*-
from collections import OrderedDict
import simplejson as json
import requests, lq_configuration

#Este script recibe un JSON con un test y lo sube a el servicio, a demás de crear el .gzip y asignar la metadata
#en AWS

def upload_test(token,test):
    endpoint = "{}v1/test/upload".format(lq_configuration.API_LQ_API)
    data = json.loads(test)
    headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)}
    print token
    try:
        response = requests.post(endpoint,json = data,headers = headers)
        #response_data = response.json()
        return response
    except Exception as e:
        return e

