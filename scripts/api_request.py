# -*- coding: utf-8 -*-
import requests
import simplejson as json
import lq_configuration

############################ ENVIAR PREGUNTA #############################
def send_question(class_,question,token):
    endpoint = "{}/v1/dashboard/test".format(lq_configuration.API_LQ_API)
    data = {"event":"professor_set_lq_next_question","payload":{"class": class_,"question": question}}
    headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)}
    response = requests.post(endpoint,json=data,headers=headers)
    print (question)
    print ('send question',response)

############################ COMENZAR PRETEST #############################
def launch_pretest(class_,pretest_id,token):
    endpoint = "{}/v1/dashboard/test".format(lq_configuration.API_LQ_API)
    data = {"event":"professor_start_pretest_series","payload":{"class": class_,"series": pretest_id}}
    headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)}
    response = requests.post(endpoint,json=data,headers=headers)
    print (pretest_id)
    print (response)

############################ DETENER PRETEST #############################
def stop_pretest(class_,pretest_id,token):
    endpoint = "{}/v1/dashboard/test".format(lq_configuration.API_LQ_API)
    data = {"event":"professor_stop_pretest_series","payload":{"class": class_,"series": pretest_id}}
    headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)}
    response = requests.post(endpoint,json=data,headers=headers)
    print (pretest_id)
    print (response)

############################ COMENZAR POSTTEST #############################
def launch_posttest(class_,posttest_id,token):
    endpoint = "{}/v1/dashboard/test".format(lq_configuration.API_LQ_API)
    data = {"event":"professor_start_posttest_series","payload":{"class": class_,"series": posttest_id}}
    headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)}
    response = requests.post(endpoint,json=data,headers=headers)
    print (posttest_id)
    print (response)

############################ DETENER POSTTEST #############################
def stop_posttest(class_,posttest_id,token):
    endpoint = "{}/v1/dashboard/test".format(lq_configuration.API_LQ_API)
    data = {"event":"professor_stop_posttest_series","payload":{"class": class_,"series": posttest_id}}
    headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)}
    response = requests.post(endpoint,json=data,headers=headers)
    print (posttest_id)
    print (response)

############################ DETENER TIEMPO DE PREGUNTA #############################
def pause_time(class_,question_id,token):
    endpoint = "{}/v1/dashboard/test".format(lq_configuration.API_LQ_API)
    data = {"event":"professor_lq_start_timer","payload":{"class": class_,"series": question_id}}
    headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)}
    response = requests.post(endpoint,json=data,headers=headers)
    print (response)

############################ REINICIAR TIEMPO DE PREGUNTA ########################
def restart_time(class_,question_id,token):
    endpoint = "{}/v1/dashboard/test".format(lq_configuration.API_LQ_API)
    data = {"event":"professor_lq_pause_timer","payload":{"class": class_,"series": question_id}}
    headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)}
    response = requests.post(endpoint,json=data,headers=headers)
    print (response)

################################# CREAR CLASE ########################################
def create_class(group_id,subject_name,subject_id,professor_name,professor_email,class_date,token):
    endpoint = "{}/v1/class/create".format(lq_configuration.API_LQ_API)
    class_year = class_date.split('-')[0]
    class_month = class_date.split('-')[1]
    class_day = class_date.split('-')[2]
    class_nextday = '{}-{}-{}'.format(class_year,class_month,int(class_day) + 1)
    # print class_date
    # print class_nextday
    # print subject_name.upper()[:5]
    data = {"class":{"name":"{}".format(subject_name),
            "start_time":"{} 00:00:00".format(class_date),
            "end_time":"{} 00:00:00".format(class_nextday),
            "package_url":"","invite_code":"{}-LQ".format(subject_name.upper()[:5]),
            "version":1,
            "class_status_type_id":1,
            "subjects_id":int(subject_id),
            "hash":"F9876567UHVI98767UHJU8UJU7R56GH",
            "class_group_id":int(group_id)
            }
            }
    headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)}
    response = requests.post(endpoint,json=data,headers=headers)
    return response

################################# CREAR ZIP CLASE ########################################
def create_zip_class(class_id,token):
    endpoint = "{}/v1/class/create/zip/{}".format(lq_configuration.API_LQ_API, class_id)
    
    headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)}
    response = requests.get(endpoint, headers=headers)
    return response

################################# OBTENER RESULTADOS TR ########################################
def get_realtime_results(class_id, series_id, question_id,token):
    endpoint = "{}/v1/class/{}/series/{}/question/{}/results".format(lq_configuration.API_LQ_API,class_id,series_id,question_id)
    response = requests.get(endpoint, headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)})
    return response.text

################################# OBTENER RESULTADOS PRETEST ########################################
def get_pretest_results(class_id,series_id,question_id,token):
    endpoint = "{}/v1/class/{}/series/{}/question/{}/results".format(lq_configuration.API_LQ_API,class_id,series_id,question_id)
    response = requests.get(endpoint, headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)})    
    return response

################################# OBTENER RESULTADOS POSTTEST ########################################
def get_posttest_results(class_id,series_id,question_id,token):
    endpoint = "{}/v1/class/{}/series/{}/question/{}/results".format(lq_configuration.API_LQ_API,class_id,series_id,question_id)
    response = requests.get(endpoint, headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)})
    return response


#print get_pretest_results(13,657,861,'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJMaXZlUXVpekJlV2ViIiwiZXhwIjoxNTY1Njc2OTAxLCJpYXQiOjE1MzQxNDA5MDEsImlzcyI6IkxpdmVRdWl6QmVXZWIiLCJqdGkiOiIyMmQ1OGYwNi1iYzYxLTQ4M2MtOTk5YS01ZjRlNjY0MWIyMTMiLCJuYmYiOjE1MzQxNDA5MDAsInN1YiI6IjE0NyIsInR5cCI6ImFjY2VzcyJ9.D6oOVWejXaahOU0B5VZj0FGKIO2SbtAIsBP8fN7kpdJsaGUbnMviNoQMGuY-8kxJ7Ci8-z2Vr5c5oySY9B2Rzg')

########################################## GET class id #################################################

def get_class_id(token):
    endpoint = "{}/v1/launch".format(lq_configuration.API_LQ_API)
    response = requests.get(endpoint, headers = {"Content-Type":"application/json","Authorization":"Bearer {}".format(token)})
    #print response.text
    return response.text
