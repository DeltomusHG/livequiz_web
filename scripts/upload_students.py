 #! /usr/bin/env python
# -*- coding: UTF-8 -*-
from collections import OrderedDict
import simplejson as json
import psycopg2
users_details = []
host_prod = 'mak-lq.c53eppnlf0by.us-east-1.rds.amazonaws.com'
host_dev = 'mak-lq-dev.c53eppnlf0by.us-east-1.rds.amazonaws.com'
host = host_prod

######################## Revisamos cada estudiante si existe en la base de datos ########################
def upload_students(students_info,class_group_name):
    new_students = []
    try:
        conn = psycopg2.connect(database = 'live_quiz_be_prod', user = 'hhddb', password = 'szqAFDVH$a77s2a9', host = host)
        cur = conn.cursor()
    except:
        print 'Imposible conectar a la base de datos.'

    def student_exist(student):
        try:
            cur.execute("""
            SELECT EXISTS (SELECT id FROM users WHERE email = '{}')
            """.format(student['email']))
            #print cur.fetchone()[0]
            validation = cur.fetchone()[0]
            if not validation:
                new_students.append(student)
                #print student
        except Exception as e:
            print e
            conn.rollback()

    map(student_exist,students_info)
    print students_info
    print new_students
    students_to_users(new_students,class_group_name,cur,conn)

######################## Revisamos cada estudiante si existe en la base de datos ########################
####################### Los estudiantes se suben a la tabla "users" #########################
def students_to_users(students_info,class_group_name,cur,conn):

    def users_query(student):
        try:
            cur.execute("""
                INSERT INTO users (email,display_name,password,is_admin,inserted_at,updated_at,user_type_id,user_status_type_id) 
                VALUES ('{}','{}','$2b$12$iwhBsr3vo215kYvhJXsrPecmHSgmp6MQQFQ4oNKOLDsbnU3kyOgsm',false,now(),now(),1,5);
                """.format(student['email'].encode('utf-8'),student['student_name'].encode('utf-8')))
            conn.commit()
        except Exception as e:
            print 'Ha habido un error'
            print e
            conn.rollback()
            return 'error'

    map(users_query,students_info) #Ejecuta el query para cada uno de los estudiantes en la lista
    students_to_users_details(students_info,class_group_name,cur,conn)
    cur.close()
    conn.close()

####################### Los estudiantes se suben a la tabla "users" #########################

################## La información de estudiantes se suben a "users_details" #################
def students_to_users_details(students_info,class_group_name,cur,conn):
    def users_details_details_insert(student):
        print student['email']
        try:
            cur.execute("""
                INSERT INTO user_details (surname,maiden_name,name,user_id,inserted_at,updated_at)
                VALUES ('{}','{}','{}',(SELECT id FROM users WHERE email = '{}'),now(),now());
                """.format(student['student_surname'].encode('utf-8'),student['student_maidenname'].encode('utf-8'),student['student_name'].encode('utf-8'),student['email']))
            conn.commit()
        except Exception as e:
            print 'error'
            print e
            conn.rollback()
    
    map(users_details_details_insert,students_info)
    
    get_users_details(students_info,class_group_name,cur,conn)
    


################## La información de estudiantes se suben a "users_details" #################

def get_users_details(students_info,class_group_name,cur,conn):
    global users_details

    def users_details_query_get(student):
        try:
            cur.execute("""
                SELECT * FROM users 
                WHERE email = '{}';""".format(student['email'].encode('utf-8')))
            
            for row in cur:
                details = OrderedDict()
                details['id'] = int(row[0])
                details['email'] = row[1]
                details['name'] = unicode(row[2],'utf-8')
                users_details.append(details)
        except:
            return 'error'

    map(users_details_query_get,students_info)
    conn.commit()
    insert_class_groups(students_info,class_group_name,cur,conn)
        


################## La información de estudiantes se suben a "users_details" #################



################## Crear el grupo con el nombre obtenido desde el front ###################

def insert_class_groups(students_info,class_group_name,cur,conn):
    try:
        conn = psycopg2.connect(database = 'live_quiz_be_prod', user = 'hhddb', password = 'szqAFDVH$a77s2a9', host = host )
        cur = conn.cursor()
    except:
        print 'Imposible conectar a la base de datos.'

    try:
        cur.execute("""
            INSERT INTO class_groups (name,inserted_at,updated_at)
            VALUES ('{}',now(),now());"""
            .format(class_group_name.encode('utf-8')))
        conn.commit()
    except:
        conn.rollback()
        return 'error'
    
    
    insert_class_groups_users(students_info,class_group_name,cur,conn)

################## Crear el grupo con el nombre obtenido desde el front ###################    
    
##################### Asociar alumnos con el grupo en class_group_usrs ###################

def insert_class_groups_users(students_info,class_group_name,cur,conn):
    try:
        conn = psycopg2.connect(database = 'live_quiz_be_prod', user = 'hhddb', password = 'szqAFDVH$a77s2a9', host = host )
        cur = conn.cursor()
    except:
        print 'Imposible conectar a la base de datos.'

    def insert_users(student):
        print student['email']
        try:
            cur.execute("""
                INSERT INTO class_group_users (class_group_id,user_id,inserted_at,updated_at)
                VALUES ((SELECT id FROM class_groups WHERE name = '{}'),(SELECT id FROM users WHERE email = '{}'),now(),now());"""
                .format(class_group_name,student['email']))
            conn.commit()
        except:
            conn.rollback()
            return 'error'
    
    map(insert_users,students_info)
    

##################### Asociar alumnos con el grupo en class_group_usrs ###################


######################### Todas las funciones ejecutándose juntas ########################

######################### Todas las funciones ejecutándose juntas ########################
